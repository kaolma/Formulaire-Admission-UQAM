﻿<?php

 require "ScriptCommun.php";
 require "ProgrammesDemandesValidations.php";
?>
<!DOCTYPE html>
	<html lang="fr">
		<head>
			<title>Programmes Demandes</title>
			<meta charset="utf-8">
			<script src="ScriptCommun.js"></script>
			<script src="ProgrammesDemandes.js"></script>
			
			<link rel="stylesheet" type="text/css" href="style/ProgrammesDemandes.css">	
				
		</head>
		<body>
		   <div id="page">
					<div id="entete">
                		<div class="identification" >PROGRAMMES DEMANDÉS</div>
                		<div id="demande"><div id="demAd">DEMANDE D'ADMISSION</div><span id="etude">Études de premier cycle</span></div>
                		<div id="numero">2</div>
                	</div>
            
            <hr class="ligne">
            <p class="petite-police">Veillez consulter les dates limites d'admission: <a id="lien-uqam" href="https://etudier.uqam.ca/dates">www.etudier.uqam.ca/dates</a></p> 
            <form method="post" onsubmit="return validerFormProgrammesDemandes();"  >
            	<div class="formulaire">     		
            			<span class="petite-police" >Je désire entreprendre mes études au trimestre</span>
						<input type= "radio" name="trimestre" value="Hiver"><span> Hiver  </span>
						<input type= "radio" name="trimestre" value="Été"><span> Été </span>
						<input type= "radio" name="trimestre" value="Automne"><span> Automne </span>  
						<input type="text" id="annee" name="annee" size="4" maxlength="4"><span>  Année </span>
						<span style="color:red" class="err" id="err-annee"></span><br>
						<span style="color:red" class="err" id="err-trimestre"></span><br>
					
					<br><br>
            		<div class="partieGauche">
            			
            			<label for="premier-choix">Premier choix (titre)</label><br>
            				<input type="text" id="premier-choix" name="premier-choix" size="60" maxlength="60"><br>
            				<span style="color:red" class="err" id="err-premier-choix"></span><br><br>
            				
            				<input type="text" id="code-premier-choix" name="code-premier-choix" size="4" maxlength="4">
            				<span style="color:red" class="err" id="err-code-premier-choix"></span><br>
							<label for="code-premier-choix">Code</label><br>
							
							<input class="choix1" type="checkbox" name="choix1" value="Baccalaureat" onclick="UneSelection('choix1',5,8)">Baccalauréat
            				<input class="choix1" type="checkbox" name="choix1" value="Majeure" onclick="UneSelection('choix1',5,8)">Majeure
            				<input class="choix1" type="checkbox" name="choix1" value="Mineure" onclick="UneSelection('choix1',5,8)">Mineure
            				<input class="choix1" type="checkbox" name="choix1" value="Certificat" onclick="UneSelection('choix1',5,8)">Certificat
            				<input class="choix1" type="checkbox" name="choix1" value="Programme-court" onclick="UneSelection('choix1',5,8)">Programme court
            				<br><span style="color:red" class="err" id="err-choix1"></span><br>
            				
            				<br><br>
						<label for="deuxieme-choix">Deuxième choix (titre)</label><br>
            				<input type="text" id="deuxieme-choix" name="deuxieme-choix" size="60" maxlength="60"><br>
            				<span style="color:red" class="err" id="err-deuxieme-choix"></span><br><br>
            				
            				<input type="text" id="code-deuxieme-choix" name="code-deuxieme-choix" size="4" maxlength="4"><br>
							<span style="color:red" class="err" id="err-code-deuxieme-choix"></span><br>
							<label for="code-deuxieme-choix">Code</label><br>
							
							
							<input class="choix2" type="checkbox" name="choix2" value="Baccalaureat2" onclick="UneSelection('choix2',5,9)">Baccalauréat
            				<input class="choix2" type="checkbox" name="choix2" value="Majeure2" onclick="UneSelection('choix2',5,9)">Majeure
            				<input class="choix2" type="checkbox" name="choix2" value="Mineure2" onclick="UneSelection('choix2',5,9)">Mineure
            				<input class="choix2" type="checkbox" name="choix2" value="Certificat2" onclick="UneSelection('choix2',5,9)">Certificat
            				<input class="choix2" type="checkbox" name="choix2" value="Programme-court2" onclick="UneSelection('choix2',5,9)">Programme court
							<br><span style="color:red" class="err" id="err-choix2"></span><br>
							
							<br><br>
						<label for="troisieme-choix">Troisième choix (titre)</label><br>
            				<input type="text" id="troisieme-choix" name="troisieme-choix" size="60" maxlength="60">
            			    <span style="color:red" class="err" id="err-troisieme-choix"></span><br><br>
            			
            			
            				<input type="text" id="code-troisieme-choix" name="code-troisieme-choix" size="4" maxlength="4"><br>
							<span style="color:red" class="err" id="err-code-troisieme-choix"></span><br>
							<label for="code-troisieme-choix">Code</label><br>
							
							<input class="choix3" type="checkbox" name="choix3" value="Baccalaureat3" onclick="UneSelection('choix3',5,10)">Baccalauréat
            				<input class="choix3" type="checkbox" name="choix3" value="Majeure3" onclick="UneSelection('choix3',5,10)">Majeure
            				<input class="choix3" type="checkbox" name="choix3" value="Mineure3" onclick="UneSelection('choix3',5,10)">Mineure
            				<input class="choix3" type="checkbox" name="choix3" value="Certificat3" onclick="UneSelection('choix3',5,10)">Certificat
            				<input class="choix3" type="checkbox" name="choix3" value="Programme-court3" onclick="UneSelection('choix3',5,10)">Programme court
							<br><span style="color:red" class="err" id="err-choix3"></span><br>
							
							<br><br>
							<div class="identification" >RENSEIGNEMENTS SUR LES ÉTUDES SECONDAIRES ET COLLÉGIALES</div>
								
           			</div>
           			<div class="partieDroite">
           				
           				<div class="temps-complet-partiel">
           					<input class="temps-choix1" type="checkbox" name="temps-choix1" value="Temps complet" onclick="UneSelection('temps-choix1',2,11)">Temps complet<br><br>
            				<input class="temps-choix1" type="checkbox" name="temps-choix1" value="Temps partiel" onclick="UneSelection('temps-choix1',2,11)">Temps partiel
           					<br><span style="color:red" class="err" id="err-temps-choix1"></span><br><br>
           					
           				</div>
           				
           				<div class="temps-complet-partiel">
           					<input class="temps-choix2" type="checkbox" name="temps-choix2" value="Temps complet" onclick="UneSelection('temps-choix2',2,12)">Temps complet<br><br>
            				<input class="temps-choix2" type="checkbox" name="temps-choix2" value="Temps partiel" onclick="UneSelection('temps-choix2',2,12)">Temps partiel
           					<br><span style="color:red" class="err" id="err-temps-choix2"></span><br><br>
           				</div>
           				
           				<div class="temps-complet-partiel">
           					<input class="temps-choix3" type="checkbox" name="temps-choix3" value="Temps complet" onclick="UneSelection('temps-choix3',2,13)">Temps complet<br><br>
            				<input class="temps-choix3" type="checkbox" name="temps-choix3" value="Temps partiel" onclick="UneSelection('temps-choix3',2,13)">Temps partiel
           					<br><span style="color:red" class="err" id="err-temps-choix3"></span><br><br>
           					
           				</div>
           			</div>
           		</div>
      
            	<hr class="ligne">
            	<p class="petite-police">Veuillez indiquer tous les programmes d'études de niveaux secondaire et collégial que
            	vous avez entrepris ou complétés.</p>
            	<p class="petite-police2">(veuillez compléter sur une feuille additionnelle jointe en annexe si l'espace est insuffisant.)</p>
           		
           			
           			<div class="partieGauche">
            			<label for="etude-secondaire-quebec">Dernière année du secondaire complétée au Québec</label><br>
            				<input type="text" id="etude-secondaire-quebec" name="etude-secondaire-quebec" size="60" maxlength="60">
            			    <br><span style="color:red" class="err" id="err-etude-secondaire-quebec"></span><br><br>
            			
            			<input class="diplome-hors" type="checkbox" name="diplome-hors" value="diplome hors Quebec coché">
            			<span class="petite-police">Diplome d'études secondaires ou collégiales poursuivies à l'extérieur du Québec</span>
            			<br><span style="color:red" class="err" id="err-diplome-hors"></span><br><br>
        
            			<label for="nom-diplome">Nom du diplôme</label><br>
            				<input type="text" id="nom-diplome" name="nom-diplome" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-nom-diplome"></span><br><br>
            				
            			<label for="discipline">Discipline ou spécialisation</label><br>
            				<input type="text" id="discipline" name="discipline" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-discipline"></span><br><br>
            			
            			<label for="institution">Institution où vous avez poursuivi vos études</label><br>
            				<input type="text" id="institution" name="institution" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-institution"></span><br><br>
            			
            			<label for="pays">Pays</label><br>
            				<input type="text" id="pays" name="pays" size="60" maxlength="60"><br>
            				<span style="color:red" class="err" id="err-pays"></span><br><br>
            				
            			<input class="dec-ou-autre" type="checkbox" name="dec-ou-autre" value="dec" onclick="UneSelection('dec-ou-autre',2,14)">
            			<span class="petite-police">DEC</span>            			
           				<input class="dec-ou-autre" type="checkbox" name="dec-ou-autre" value="autre-diplome" onclick="UneSelection('dec-ou-autre',2,14)">
           				<span class="petite-police">AUTRE diplôme de niveau collégial(AEC, CEC,...)</span>
           				<br><span style="color:red" class="err" id="err-dec-ou-autre"></span><br><br>
           				
           				<label for="nom-diplome-dec">Nom du diplôme</label><br>
            				<input type="text" id="nom-diplome-dec" name="nom-diplome-dec" size="60" maxlength="60"><br>
            				<span style="color:red" class="err" id="err-nom-dec"></span><br><br>


						<label for="discipline-dec">Discipline ou spécialisation</label><br>
							<input type="text" id="discipline-dec" name="discipline-dec" size="60" maxlength="60"><br>
						    <span style="color:red" class="err" id="err-discipline-dec"></span><br><br>
						
						
						<label for="institution-dec">Institution où vous avez poursuivi vos études en vue de l'obtention de ce diplôme</label><br>	
							<input type="text" id="institution-dec" name="institution-dec" size="60" maxlength="60"><br>
							<span style="color:red" class="err" id="err-institution-dec"></span><br><br>
							
							
						<p class="petite-police">Si les sections suivantes ne s'appliquent pas à votre cas, passez à la dernière page du formulaire.</p>
           			</div>
       				
       				<div class="partieDroite">
       					<div>Période de fréquentation</div>
						<input type="text" id="annee-debut-quebec" name="annee-debut-quebec" size="2" maxlength="2"><span> De (année)</span>
						<input type="text" id="annee-fin-quebec" name="annee-fin-quebec" size="2" maxlength="2"><span> à (année)</span><br>           				
						<span style="color:red" class="err" id="err-annee-debut-qc"></span><br>
						<span style="color:red" class="err" id="err-annee-d-qc"></span><br>
						<span style="color:red" class="err" id="err-annee-f-qc"></span><br>						
						
						<div class="annee-debut">Période de fréquentation</div>
						<input type="text" id="annee-debut" name="annee-debut" size="2" maxlength="2"><span> De (année)</span>
						<input type="text" id="annee-fin" name="annee-fin" size="2" maxlength="2"><span> à (année)</span><br>           				
						<span style="color:red" class="err" id="err-annee-debut"></span><br>
						<span style="color:red" class="err" id="err-annee-d"></span><br>
						<span style="color:red" class="err" id="err-annee-f"></span><br>
						
						<div class="date-obtention">Date d'obtention</div>
						<input type="text" id="mois-obtention" name="mois-obtention" size="2" maxlength="2"><span> Mois </span>
						<input type="text" id="annee-obtention" name="annee-obtention" size="2" maxlength="2"><span> Année </span>           				
						<br><span style="color:red" class="err" id="err-mois-obt"></span><br>
						<span style="color:red" class="err" id="err-annee-obt"></span><br>
						
						
						<div class="obtention">
           					<input class="obtention-dec" type="checkbox" name="obtention-dec" value="obtenu" onclick="UneSelection('obtention-dec',3,15)">
           					<span> Obtenu </span> 
            				<input class="obtention-dec" type="checkbox" name="obtention-dec" value="a-obtenir" onclick="UneSelection('obtention-dec',3,15)">
            				<span> À obtenir </span>
           					<input class="obtention-dec" type="checkbox" name="obtention-dec" value="non-obtenu" onclick="UneSelection('obtention-dec',3,15)">
           					<span> Ne sera pas obtenu </span><br>
           					<span style="color:red" class="err" id="err-obtention-dec"></span><br>
           					
           					
           				</div>
						<div class="annee-debut">Période de fréquentation</div>
						<input type="text" id="annee-debut-dec" name="annee-debut-dec" size="2" maxlength="2"><span> De (année)</span>
						<input type="text" id="annee-fin-dec" name="annee-fin-dec" size="2" maxlength="2"><span> à (année)</span><br>           				
						<span style="color:red" class="err" id="err-annee-debut-dec"></span><br>
						<span style="color:red" class="err" id="err-annee-d-dec"></span><br>
						<span style="color:red" class="err" id="err-annee-f-dec"></span><br>
						
						
						<div class="date-obtention">Date d'obtention</div>
						<input type="text" id="mois-obtention-dec" name="mois-obtention-dec" size="2" maxlength="2"><span> Mois </span>
						<input type="text" id="annee-obtention-dec" name="annee-obtention-dec" size="2" maxlength="2"><span> Année </span><br>           				
           				<span style="color:red" class="err" id="err-mois-obt-dec"></span><br>
						<span style="color:red" class="err" id="err-annee-obt-dec"></span><br><br><br><br>
						
           				<input type="submit" value="Page suivante" name= "submit"/>
           				
           				
           			</div>
           			
           		</form>
           		
       
           	</div>
		</body>
	</html>
