﻿
var prevIndex=[-1,-1];

function effacerErreurs(){
    erreurs=document.getElementsByClassName("err"); 
    var e; 
    for(e=0; e<erreurs.length; e++){
        erreurs[e].innerHTML = ""; 
    }
}

function champObligatoire(inputId, divId, nomChamp){
    var element = document.getElementById(inputId).value;
    if (element == null || element === "") {
        document.getElementById(divId).innerHTML = "* Le champ " + nomChamp +" est obligatoire!";
        return false;
    }
    return true;
}

function validationBoutonRadio(inputName, divId, nomChamp){
    var valeur="";
    var valeurExiste=true;
    try{
        valeur=document.querySelector(inputName).value;
    }
    catch(err){
        document.getElementById(divId).innerHTML ="* Le champs "+nomChamp+" est obligatoire";
        valeurExiste=false;
    }
    return valeurExiste;
}

function validerChampRadioAutre(bouleenne,radioId,inputId,divId,nomChamp){
    if(bouleenne){
        if (document.getElementById(radioId).checked){
            return champObligatoire(inputId,divId,nomChamp);   
        }
    }
}

function estBissextile(annee) {
    return ((annee % 4 === 0 && annee % 100 > 0) || (annee % 400 === 0));
}

function getValue(inputId){
    return document.getElementById(inputId).value; 
}

function validerDate(inputId,spanId1,spanId2,spanId3){
    var formatAnnee = true;
    var anneeValide = true;
    var moisValide  = true;
    var jourValide  = true;
    var fevrier29   = true;
    var fevrier28   = true;
    var valeur = document.getElementById(inputId).value; 
    var regexp = /[0-9]{4}-[0-9]{2}-[0-9]{2}/; 
    var annee = parseInt(valeur.substring(0,4));
    var mois = parseInt(valeur.substring(5,7));
    var jour = parseInt(valeur.substring(8,10));
    var dateCourante=new Date();
    var anneeCourante = dateCourante.getFullYear();
    anneeBissextile= estBissextile(annee);
    
    if(!regexp.test(valeur)){
        document.getElementById(spanId1).innerHTML = "La date doit être du format AAAA-MM-JJ";
        formatAnnee=false;
    }
    if(annee>(anneeCourante-15)){
        document.getElementById(spanId2).innerHTML = "l'année minimale pour une inscription est: "+(anneeCourante-15);
        anneeValide= false;
    }        
    if(mois<1||mois>12){
        document.getElementById(spanId2).innerHTML = "Veuillez entrer un mois valide  ";
        moisValide= false;
    }
    if (jour<1||jour>31){
        document.getElementById(spanId2).innerHTML = "Veuillez entrer un jour valide  ";
        jourValide= false;
    }
    if (anneeBissextile){ 
       if(mois==2 && jour>29){        
            document.getElementById(spanId3).innerHTML = "Veuillez entrer un jour valide pour votre année et mois de naissance ";
            fevrier28 = false;
        }
    }
    if (!anneeBissextile){  
        if(mois==2 && jour>28){        
            document.getElementById(spanId3).innerHTML = "Veuillez entrer un jour valide pour votre mois de naissance ";
            fevrier29 = false;
        }
    } 
    return formatAnnee && anneeValide &&moisValide &&jourValide &&fevrier29 &&fevrier28;
}

function validerCodePostal(inputId,spanId,msg){
    if(!champVide(inputId,spanId,msg)){
        var valeur  = document.getElementById(inputId).value;
        var lettre1 = valeur.substring(0,1);
        var chiffre1 = valeur.substring(1,2);
        var lettre2 = valeur.substring(2,3);
        var espace = valeur.substring(3,4);
        var chiffre2 = valeur.substring(4,5);
        var lettre3 = valeur.substring(5,6);
        var chiffre3 = valeur.substring(6,7);
        chiffre1 = parseInt(chiffre1);
        chiffre2 = parseInt(chiffre2);
        chiffre3 = parseInt(chiffre3);
        var formatCp = (lettre1>='A' && lettre1<='Z') &&  (chiffre1>=0 && chiffre1<=9)
        &&(lettre2>='A' && lettre2<='Z') &&  (chiffre2>=0 && chiffre2<=9)
        &&(lettre3>='A' && lettre3<='Z') &&  (chiffre3>=0 && chiffre3<=9) && espace === " "; 

        if(!formatCp){        
            document.getElementById(spanId).innerHTML = "Le code postal doit avoir le format: X9X 9X9";
            return false; 
        }
        return true; 
    }
}
function validerNumeroTelephone(inputId,spanId){
    
    var valeur  = document.getElementById(inputId).value;
    var regexp = /[0-9]{3}-[0-9]{3}-[0-9]{4}/; 
    if(!regexp.test(valeur)){ 
        document.getElementById(spanId).innerHTML = "Le numéro de téléphone saisi doit avoir le format: 999-999-9999";
        return false; 
    }
    return true;     
}
function champVide(inputId, spanId, msg){
    var champVide=false;
    if(document.getElementById(inputId).value === ""){
        document.getElementById(spanId).innerHTML = msg;
        champVide=true;
    }
    return champVide;
}

function minimumUnNumber(){
    var numeroManquant=true;
    if (!champVide("tel-dom","err-numero1" ,"")
            || !champVide("tel-cel", "err-numero2","")
            ||!champVide("tel-trv", "err-numero3", "")){

        if(!champVide("tel-dom","err-numero1" ,"")){
            validerNumeroTelephone("tel-dom","err-numero1");   
        }

        if(!champVide("tel-cel", "err-numero","")){
            validerNumeroTelephone("tel-cel","err-numero2");
        }
        if(!champVide("tel-trv", "err-numero", "")){
            validerNumeroTelephone("tel-trv","err-numero3");
        }
        numeroManquant=false;
    }
    if(numeroManquant){      
        document.getElementById("err-numero").innerHTML = "* En moins un des 3 numéros doit être saisi"; 
    }
    return numeroManquant; 
}
function validerCodePermanent(inputId,spanId,msg){
    if (!champVide(inputId,spanId,msg)){
        var valeur = getValue(inputId);
        var regexp = /[A-Z]{4}[0-9]{8}/;
        if(!regexp.test(valeur)){ 
            document.getElementById(spanId).innerHTML = "* Le code permanent doit avoir le format: XXXX99999999";
            return false; 
        }
        return true;  
    }
}
function validerNas(inputId,spanId,msg){
    if(!champVide(inputId,spanId,msg)){
        var valeur = getValue(inputId);
        for (var i=0; i<valeur.length;i++){
            if (valeur.length != 9 || (valeur[i]<'0' || valeur[i]>'9')){
                 document.getElementById(spanId).innerHTML = "* Le numéro d'assurance social doit être composé de 9 chiffres";
                return false; 
            }
        }
        return true;  
    }
}
function validerParent(inputId1,inputId2,spanId,msg1,msg2){
    if(!champVide(inputId1,spanId,msg1)){
        champVide(inputId2,spanId,msg2);
    }else if(!champVide(inputId2,spanId,msg1)){
        champVide(inputId1,spanId,msg2);
        
    }
}
function validerCourriel(inputId,spanId,msg){
    if(!champVide(inputId,spanId,msg)){
        var courriel=document.getElementById("courriel").value;
        if (((courriel.indexOf("@")>=0)&&(courriel.indexOf(".")>=0))){
            var debut  = courriel.substring(0,courriel.indexOf('@')).length;
            var milieu = courriel.substring(debut+1,courriel.indexOf('.')).length;
            var fin    = courriel.substring(debut+1+milieu+1).length;       
            if(debut===0 || milieu===0 || fin==0){
                document.getElementById(spanId).innerHTML = "* Le courriel doit avoir le format xxxxxxx@xxxxxxxx.xxx ";
                return false; 
            }
        }else{
            document.getElementById(spanId).innerHTML = "* Le courriel doit contenir les caractères '@' et ' . ' !";
            return false; 
        }
    }
    return true;
}

function validerForm(){   
    
    var autreCitoyennete, autreLangueUsage, autreLangueMaternelle, codePostal1, codePostal2,
            nomFamille, prenom, codePermanent1, codePermanent2, citoyennete, lieuNaissance, nas,
            adresse, sexe, statut, dateNaissance, langueUsage, langueMaternelle, minimumUnNumero,
            pere, mere, courriel;
    
    effacerErreurs();
    
    nomFamille           = champObligatoire("nom","err-nom","Nom de famille");
    prenom               = champObligatoire("prenom","err-prenom", "Prénom usuel");
    codePermanent2       = champObligatoire("code-permanent2","err-cp2", "Code permanent du ministère")&& validerCodePermanent("code-permanent2","err-cp2","");
    codePermanent1       = validerCodePermanent("code-permanent1","err-cp1","");    
    citoyennete          = validationBoutonRadio("input[name=citoyennete]:checked","err-citoyennete","Citoyenneté");
    autreCitoyennete     = validerChampRadioAutre(citoyennete,"autre-citoyennete","champ-autre-citoyennete","err-autre-citoyennete","Autre citoyenneté");
    lieuNaissance        = champObligatoire("lieu-naissance","err-lieu-naissance","Lieu de naissance");
    adresse              = champObligatoire("adresse","err-adresse" , "Adresse"); 
    sexe                 = validationBoutonRadio("input[name=sexe]:checked","err-sexe", "Sexe");
    statut               = validationBoutonRadio("input[name=statut]:checked","err-statut", "Statut au Canada");
    dateNaissance        = champObligatoire("date-naissance","err-date-naissance","Date de naissance")
                            && validerDate("date-naissance","err-date-naissance","err-date-naissance2","err-date-naissance3");
    langueUsage          = validationBoutonRadio("input[name=langue-usage]:checked","err-langue-usage","Langue d'usage");    
    autreLangueUsage     = validerChampRadioAutre(langueUsage,"autre-langue-usage","champ-autre-usage","err-langue-usage","Autre langue d'usage");
    langueMaternelle     = validationBoutonRadio("input[name=langue-maternelle]:checked","err-langue-mat","Langue maternelle ");
    autreLangueMaternelle= validerChampRadioAutre(langueMaternelle,"autre-langue-maternelle","champ-autre-maternelle","err-langue-mat","Autre langue Maternelle");
    codePostal1          = validerCodePostal("code-postal1","err-code-postal1","");
    codePostal2          = validerCodePostal("code-postal2","err-code-postal2","");
    minimumUnNumero      = minimumUnNumber();
    nas                  = validerNas("nas","err-nas","");
    pere                 = validerParent("nom-pere","prenom-pere","err-pere","","Les informations du père sont incomplètes!");
    mere                 = validerParent("nom-mere","prenom-mere","err-mere","","Les informations de la mère sont incomplètes!");   
    courriel             = validerCourriel("courriel","err-courriel","");
    
    return nomFamille && prenom && codePermanent1 && codePermanent2&& lieuNaissance && adresse && citoyennete 
           && autreCitoyennete && sexe && statut && dateNaissance && langueUsage && langueMaternelle && minimumUnNumero 
           && autreLangueMaternelle && autreLangueUsage && codePostal1 && codePostal2 && minimumUnNumero &&nas && pere && mere &&courriel;
    }
