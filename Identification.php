<?php
require 'IdentificationValidations.php';
?>
<!Doctype html>
<html>
    <head>
        <meta charset = "utf-8">
        <title>Identification</title>
        <link rel="stylesheet" type="text/css" href="style/identification.css"> 
        <script src="identification.js"></script>
    </head>
    <body>
        <div id="page">
            <div id="entete">
                <div id="identification" >IDENTIFICATION</div>
                <div id="demande"><div id="demAd">DEMANDE D'ADMISSION</div><span id="etude">Études de premier cycle</span></div>
                <div id="numero">1</div>
            </div>
            
            <hr id="ligne">
            
            <div id="formulaire">
               
                <form onsubmit="return validerForm();" method="post" action="Identification.php">
                   
                    <div id="partieGauche">
                        <label for="nom">Nom de famille à la naissance</label><br>
                        <input type="text" id="nom" name="nom" size="60" maxlength="60">
                        <div style="color:red" class="err" id="err-nom"></div><br><br>
                        
                        <label for="prenom">Prénom usuel</label><br>
                        <input type="text" id="prenom" name="prenom" size="60" maxlength="60">
                        <div style="color:red" class="err" id="err-prenom"></div><br><br><br><br><br>
                       
                        <label for="code-permanent1">Code permanent (alphanumérique)si vous avez déjà étudié à l'UQAM</label><br>
                        <input type="text" id="code-permanent1" name="code-permanent1" maxlength="12" placeholder="XXXX99999999"><br><br>
                        <div style="color:red" class="err" id="err-cp1"></div><br><br>
                        <label for="code-permanent2">Code permanent (alphanumérique)du ministère de l'Éducation et de l'Enseignement supérieur</label><br>
                        <input type="text" id="code-permanent2" name="code-permanent2" maxlength="12" placeholder="XXXX99999999"><br><br>
                        <div style="color:red" class="err" id="err-cp2"></div><br><br>
                        
                        <label>Citoyenneté</label><br>
                        <input type="radio" class="case" name="citoyennete" id="canada" value="canadienne">
                        <span class="petite-police">Canadienne</span>
                        <input type="radio" class="case" name="citoyennete"  id="autre-citoyennete" value="autre">
                        <span class="petite-police">Autre(précisez)</span><br>
                        <div style="color:red" class="err" id="err-citoyennete"></div><br><br>
                        <input type="text" id="champ-autre-citoyennete" name="autre-citoyennete" size="20"><br><br>
                        <div style="color:red" class="err" id="err-autre-citoyennete"></div><br><br>
                        
                        <label for="lieu-naissance">Lieu de naissance (ville)</label><br>
                        <input type="text" id="lieu-naissance" name="lieu-naissance" size="40"><br><br>
                        <div style="color:red" class="err" id="err-lieu-naissance"></div><br><br>
                        
                        <label for="nom-pere">Nom de famille du père à la naissance</label><br>
                        <input type="text" id="nom-pere" name="nom-pere" size="40"><br><br>    
                        <label for="prenom-pere">Prénom usuel du père</label><br>
                        <input type="text" id="prenom-pere" name="prenom-pere" size="40"><br><br>
                        <div style="color:red" class="err" id="err-pere"></div><br><br>
                        
                        <label for="nom-mere">Nom de famille de la mère à la naissance</label><br>
                        <input type="text" id="nom-mere" name="nom-mere" size="40"><br><br>
                        <label for="prenom-mere">Prénom usuel de la mère</label><br>
                        <input type="text" id="prenom-mere" name="prenom-mere" size="40"><br><br>
                        <div style="color:red" class="err" id="err-mere"></div><br><br>
                        
                        <label for="tel-dom">Téléphone à domicile</label><br>
                        <input type="text" id="tel-dom" name="tel-dom" maxlength="12"><br><br>
                        <div style="color:red" class="err" id="err-numero1"></div><br>
                        <label for="tel-cel">Cellulaire</label><br>
                        <input type="text" id="tel-cel" name="tel-cel" maxlength="12"><br><br>
                        <div style="color:red" class="err" id="err-numero2"></div><br>
                        <label for="tel-trv">Téléphone au travail</label><br>
                        <input type="text" id="tel-trv" name="tel-trv" maxlength="12">
                        <span class="petite-police">poste</span><input type="text" id="poste" name="poste" size="3"><br>
                        <div style="color:red" class="err" id="err-numero3"></div><br>
                        <div style="color:red" class="err" id="err-numero"></div><br>
                       
                        <label>Courriel</label><br>
                        <input type="text" id="courriel" name="courriel" placeholder="xxxxxx@xxxxxx.xxx" size="40"><br><br>
                        <div style="color:red" class="err" id="err-courriel"></div><br>
                        
                        <label for="adresse">Adresse de correspondance </label><br>
                        <input type="text" size="73" id="adresse"  name="adresse" 
                               placeholder="N° civique            Type et nom de la rue                            Direction de la rue"> 
                        
                        <input type="text" id="num-appt1" name="num-appt1" size="5" placeholder="N° appt">
                        <div style="color:red" class="err" id="err-adresse"></div><br>
                        <input type="text" size="60" id="mun" name="mun" 
                               placeholder="Municipalité                        Pays (si autre que le Canada)"><br><span class="petite-police">Code postal</span>
                        <input type="text" id="code-postal1" maxlength="7" name="code-postal1"  placeholder="X9X 9X9"><br>
                        <div style="color:red" class="err" id="err-code-postal1"></div><br>
                        <br>
                        
                        <label for="adresse-residence">Adresse ou vous résidez actuellement
                        (si different de l'adresse de correspondance)</label><br>
                        <input type="text" size="73" id="adresse-residence" name="adresse2" 
                               placeholder="N° civique             Type et nom de la rue                           Direction de la rue"> 
                        <input type="text" id="num-appt2" name="num-appt2" size="5" placeholder="N° appt"><br><br>
                        <input type="text" size="60" id="mun2" name="mun2"
                               placeholder="Municipalité                        Pays (si autre que le Canada)"><br><span class="petite-police">Code postal</span>
                        <input type="text" id="code-postal2" name="code-postal2" maxlength="7" placeholder="X9X 9X9"><br>
                        <div style="color:red" class="err" id="err-code-postal2"></div><br>
                        
                        <input id="page-suivante" type="submit" value="Page suivante">
                    </div>
                    
                    <div id="partieDroite">
                        <label for="date-naissance">Date de naissance</label><br>
                        <input type="text" id="date-naissance" name="date-naissance" placeholder="AAAA-MM-JJ"><br>
                        <div  style="color: red" class="err" id="err-date-naissance"></div><br>
                        <div  style="color: red" class="err" id="err-date-naissance2"></div><br>
                        <div  style="color: red" class="err" id="err-date-naissance3"></div><br>
                        <label>Sexe</label><br>
                        <input type="radio" class="case" name="sexe" value="feminin">
                        <span class="petite-police">Féminin</span>
                        <input type="radio" class="case" name="sexe" value="masculin">
                        <span class="petite-police">Masculin</span><br><br>
                        <div  style="color: red" class="err" id="err-sexe"></div><br>
                        
                        <label for="nas">N° d'assurance sociale</label><br>
                        <input type="text" id="nas" name="nas" maxlength="9">
                        <p class="petite-police">Le NAS n'est pas obligatoire. Il est requis<br>si vous désirez
                         recevoir un reçu pour fins d'impôt.</p>
                        <div  style="color: red" class="err" id="err-nas"></div><br>
                        <fieldset>
                            <legend>Statut au Canada</legend>
                            <div id="state">
                                <div id="gauche">
                                    <input type="radio" name="statut" value="citoyen canadien">
                                    <span class="petite-police">Citoyen Canadien</span><br>
                                    <input type="radio" name="statut" value="amérindien">
                                    <span class="petite-police">Amérindien</span><br>
                                    <input type="radio" name="statut" value="résident permanent">
                                    <span class="petite-police">Résident permanent</span><br>
                                    <input type="radio" name="statut" value="visa diplomatique">
                                    <span class="petite-police">Visa diplomatique</span><br>
                                </div>
                                <div id="droite">
                                    <input type="radio" name="statut" value="permis d'études">
                                    <span class="petite-police">Permis d'études</span><br>
                                    <input type="radio" name="statut" value="permis de séjour temporaire">
                                    <span class="petite-police">Permis de séjour temporaire</span><br>
                                    <input type="radio" name="statut" value="permis de travail">
                                    <span class="petite-police">Permis de travail</span><br>
                                    <input type="radio" name="statut" value="réfugié">
                                    <span class="petite-police">Réfugié</span><br>
                                </div>
                            </div>
                            <p class="petite-police">Si vous n'êtes pas nés au Québec, veuillez 
                            vous réféfer au texte de l'encadré gris de la page 4.</p>
                            <div  style="color: red" class="err" id="err-statut"></div><br>
                        </fieldset><br><br>
                        
                        <label>Langue d'usage(Langue parlée le plus souvent à la maison)</label><br><br>
                        
                        <input type="radio" name="langue-usage" class="case" value="français">
                        <span>Français</span><br>
                        <input type="radio" name="langue-usage" class="case" value="anglais">
                        <span>Anglais</span><br>
                        <input type="radio" name="langue-usage" class="case" value="amérindien">
                        <span>Amérindien ou Inuktitut</span><br>
                        <input type="radio" name="langue-usage" class="case"  id="autre-langue-usage" value="autre">
                        <span>Autre(precisez)&nbsp;</span>
                        
                        <input type="text" id="champ-autre-usage" name="champ-autre-usage" size="20"><br><br>
                        <div  style="color: red" class="err" id="err-langue-usage"></div><br>
                        
                        <label>Langue maternelle (Première langue apprise et encore comprise)</label><br><br>
                        <input type="radio" name="langue-maternelle" class="case" value="francais">
                        <span>Français</span><br>
                        <input type="radio" name="langue-maternelle" class="case" value="anglais">
                        <span>Anglais</span><br>
                        <input type="radio" name="langue-maternelle" class="case" value="amerindien">
                        <span>Amérindien ou Inuktitut</span><br>
                        <input type="radio" name="langue-maternelle" class="case" id="autre-langue-maternelle" value="autre">
                        <span>Autre(precisez)&nbsp;</span>
                        
                        <input type="text" size="20" id="champ-autre-maternelle" name="champ-autre-maternelle"><br><br>
                        <div  style="color: red" class="err" id="err-langue-mat"></div><br>
                    </div>
                </form>
              
            </div>
        </div>
       
    </body>
</html>
