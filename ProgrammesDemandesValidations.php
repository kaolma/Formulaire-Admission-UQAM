﻿<?php
session_start();




function redirection(){
	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		
		if(verificationIsset()){
			if(champsObligatoires()
					|| champsACompleter('deuxieme-choix', 'code-deuxieme-choix', 'choix2', 'temps-choix2')
					|| champsACompleter('troisieme-choix', 'code-troisieme-choix', 'choix3', 'temps-choix3')
					|| validationAnneeTrimestre()
					|| validationSecondaireQc()
					|| validationDiplomeHorsQc()
					|| validationDiplomeQcDec()){
						
						http_response_code(400);
						echo "400 Bad Request ";
						exit();
						
			}else{
				ecritureFichier();
				header("Location: RenseignementsEtudes.php", true, 303);
			}
			
		}else{
			http_response_code(400);
			echo "400 Bad Request ";
			exit();
			
		}
	}
	
}

function verificationIsset(){
	
	
	return  isset($_POST['trimestre'])|| isset($_POST['annee']) || isset($_POST['premier-choix'])
	|| isset($_POST['code-premier-choix']) || isset($_POST['choix1']) || isset($_POST['temps-choix1'])
	|| isset($_POST['deuxieme-choix']) || isset($_POST['code-deuxieme-choix']) || isset($_POST['choix2'])
	|| isset($_POST['temps-choix2']) || isset($_POST['troisieme-choix']) || isset($_POST['code-troisieme-choix'])
	|| isset($_POST['choix3']) || isset($_POST['temps-choix3']) || isset($_POST['etude-secondaire-quebec'])
	|| isset($_POST['annee-debut-quebec']) || isset($_POST['annee-fin-quebec']) || isset($_POST['diplome-hors'])
	|| isset($_POST['nom-diplome']) || isset($_POST['discipline']) || isset($_POST['institution'])
	|| isset($_POST['pays']) || isset($_POST['annee-debut']) || isset($_POST['annee-fin'])
	|| isset($_POST['mois-obtention']) || isset($_POST['annee-obtention']) || isset($_POST['dec-ou-autre'])
	|| isset($_POST['nom-diplome-dec']) || isset($_POST['discipline-dec']) || isset($_POST['institution-dec'])
	|| isset($_POST['obtention-dec']) || isset($_POST['annee-debut-dec']) || isset($_POST['annee-fin-dec'])
	|| isset($_POST['mois-obtention-dec']) || isset($_POST['annee-obtention-dec']);
	
}

function champsObligatoires(){
	return 	  (empty($_POST['trimestre'])
			|| empty($_POST['annee'])
			|| empty($_POST['premier-choix'])
			|| empty($_POST['code-premier-choix'])
			|| empty($_POST['choix1'])
			|| empty($_POST['temps-choix1']));
}



function champsACompleter($choix, $code, $niveau, $tempsChoix){
	
	return ! ((!empty($_POST[$choix])
			&& !empty($_POST[$code])
			&& !empty($_POST[$niveau])
			&& !empty($_POST[$tempsChoix]))
			||
			(empty($_POST[$choix])
					&& empty($_POST[$code])
					&& empty($_POST[$niveau])
					&& empty($_POST[$tempsChoix])));
}

function validationDiplomeHorsQc(){
	
	if(!empty($_POST['diplome-hors']) || !empty($_POST['nom-diplome'])
			|| !empty($_POST['discipline']) || !empty($_POST['institution'])
			|| !empty($_POST['pays']) || !empty($_POST['annee-debut'])
			|| !empty($_POST['annee-fin']) || !empty($_POST['mois-obtention'])
			|| !empty($_POST['annee-obtention'])){
				if(empty($_POST['diplome-hors']) || empty($_POST['nom-diplome'])
						|| empty($_POST['discipline']) || empty($_POST['institution'])
						|| empty($_POST['pays']) || empty($_POST['annee-debut'])
						|| empty($_POST['annee-fin']) || empty($_POST['mois-obtention'])
						|| empty($_POST['annee-obtention'])
						|| verificationAnneeFreq('annee-debut', 'annee-fin')
						|| validationMois('mois-obtention')
						|| validationAnnee('annee-obtention')){
							return true;
				}
				
	}
	return false;
}
function validationAnneeTrimestre(){
	if (intVal(date('Y'))>intVal($_POST['annee'])){
		return true;
	}
	return false;
}


function validationSecondaireQc(){
	if(!empty($_POST['etude-secondaire-quebec'])
			|| !empty($_POST['annee-debut-quebec'])
			|| !empty($_POST['annee-fin-quebec'])){
				if(empty($_POST['etude-secondaire-quebec'])
						|| empty($_POST['annee-debut-quebec'])
						|| empty($_POST['annee-fin-quebec'])
						|| verificationAnneeFreq('annee-debut-quebec', 'annee-fin-quebec')){
							return true;
				}
	}
	return false;
}
function validationDiplomeQcDec(){
	if(!empty($_POST['dec-ou-autre']) || !empty($_POST['obtention-dec'])
			|| !empty($_POST['nom-diplome-dec'])
			|| !empty($_POST['discipline-dec'])
			|| !empty($_POST['institution-dec'])
			|| !empty($_POST['annee-debut-dec']) || !empty($_POST['annee-fin-dec'])
			|| !empty($_POST['mois-obtention-dec'])
			|| !empty($_POST['annee-obtention-dec'])){
				if(empty($_POST['dec-ou-autre']) || empty($_POST['obtention-dec'])
						|| empty($_POST['nom-diplome-dec'])
						|| empty($_POST['discipline-dec'])
						|| empty($_POST['institution-dec'])
						|| empty($_POST['annee-debut-dec'])|| empty($_POST['annee-fin-dec'])
						|| empty($_POST['mois-obtention-dec'])
						|| empty($_POST['annee-obtention-dec'])
						|| verificationAnneeFreq('annee-debut-dec', 'annee-fin-dec')
						|| validationMois('mois-obtention-dec')
						|| validationAnnee('annee-obtention-dec')){
							return true;
				}
				
	}
	return false;
	
	
}


function ecritureFichier(){
	
	$fichierFormulaire = fopen($_SESSION['fichier_code_permanent'], 'a+');
	if(!$fichierFormulaire){
		echo "Le fichier n'est pas ouvert";
	}
	else{
		
		$contenu= "\n"."Trimestre de demande d'étude: " . $_POST['trimestre']. " de l'annee ". $_POST['annee']."\n"
				. "Premier choix: ". $_POST['premier-choix']."\n"
				. "Code premier choix: ". $_POST['code-premier-choix']."\n"
				. "Niveau d'étude: ". $_POST['choix1']."\n"
				. "Horaire: ". $_POST['temps-choix1']."\n"
				. "Deuxième Choix: ". $_POST['deuxieme-choix']."\n"
				. "Niveau d'étude: ". $_POST['choix2']."\n"
				. "Horaire: ". $_POST['temps-choix2']."\n"
				. "Troisième choix: ". $_POST['troisieme-choix']."\n"
				. "Code troisième choix: ". $_POST['code-troisieme-choix']."\n"
				. "Niveau d'étude: ". $_POST['choix3']."\n"
				. "Horaire: ". $_POST['temps-choix3']."\n\n"
			    . "RENSEIGNEMENTS SUR LES ÉTUDES SECONDAIRES ET COLLÉGIALES"."\n\n"
				. "Dernière année du secondaire complété au Québec: ".$_POST['etude-secondaire-quebec']."\n"
				. "Période de fréquentation : De(Année) ". $_POST['annee-debut-quebec']." à (Année) ". $_POST['annee-fin-quebec']."\n"
				. "Diplôme d'études secondaires ou collégial poursuivi à  l'extérieur du Québec: ". $_POST['diplome-hors']."\n"
				. "Nom du diplôme: ". $_POST['nom-diplome']."\n"
				. "Discipline ou spécialisation: ". $_POST['discipline']."\n"
				. "Institution ou vous avez poursuivi vos études: ". $_POST['institution']."\n"
				. "Pays: ". $_POST['pays']."\n"
				. "Période de fréquentation: De(Année) ". $_POST['annee-debut']." à (Année) ". $_POST['annee-fin']."\n"
				. "Date d'obtention: (mois) ". $_POST['mois-obtention']." (Année) ". $_POST['annee-obtention']."\n"
				. "DEC ou Autre diplôme de niveau collégial(AEC,CEC,...): ". $_POST['dec-ou-autre']."\n"
				. "Nom du diplôme: ". $_POST['nom-diplome-dec']."\n"
				. "Discipline ou spécialisation: ". $_POST['discipline-dec']."\n"						
				. "Institution ou vous avez poursuivi vos études: ". $_POST['institution-dec']."\n"
			    . "Étât de l'obtention: ". $_POST['obtention-dec']."\n"
				. "Période de fréquentation: De(Année) ". $_POST['annee-debut-dec']." à (Année)  ". $_POST['annee-fin-dec']."\n"
			    . "Date d'obtention: (mois) ". $_POST['mois-obtention-dec']." (Année) ". $_POST['annee-obtention-dec']."\n";	
		
				fwrite($fichierFormulaire,$contenu);
				fclose($fichierFormulaire);
	}
}

redirection();




?>