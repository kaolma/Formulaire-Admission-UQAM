<?php
require "RenseignementsSupplementairesValidations.php";
?>
<!DOCTYPE html>
	<html lang="fr">
		<head>
			<title>RENSEIGNEMENTS</title>
			<meta charset="utf-8">
			<link rel="stylesheet" type="text/css" href="style/ProgrammesDemandes.css">	
		</head>
		<body>
			<div id="page">
				<div id="entete">
                	<div class="identification" >RENSEIGNEMENTS SUPPLÉMENTAIRES</div>
                	<div id="demande"><div id="demAd">DEMANDE D'ADMISSION</div><span id="etude">Études de premier cycle</span></div>
                	<div id="numero">4</div>
                </div>	
              	<hr class="ligne">
              	<span class="petite-police">Veuillez, indiquer toute autre expérience, différente d'un emploi, qui vous semble pertinente à votre demande d'admission.
              	Il peut s'agir de publications, de recherches, de contributions intellectuelles ou professionnelles.</span><br><br>
              	<form  method="post"  >
              		<textarea name="renseignements-supplementaires" rows="40" cols="100"> </textarea>
              		
              		<div class="partieDroite">
              			<input class="page-suivante" type="submit" value="Soumission de la demande" name= "submit"/>	
              		</div>
              		
				</form>
			</div>
		</body>
	</html>