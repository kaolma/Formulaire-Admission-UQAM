﻿var	prevIndex=[-1,-1];

function effacerErreurs(){ 

	var errs = document.getElementsByClassName("err"); //retourne un tableau de tous les span
	var e;
	for(e=0;e<errs.length;e++){
		errs[e].innerHTML="";
	}
	
}


function UneSelection(classNom, nbcheckbox, tabId){
	var tableau =document.getElementsByClassName(classNom);

	if( prevIndex[tabId] >= 0){
		tableau[prevIndex[tabId]].checked =false;
		prevIndex[tabId] = -1;
	}

	for(var i=0; i<nbcheckbox; i++){
		if(tableau[i].checked)
			prevIndex[tabId] = i;
	}

}


function validationDates(dateDebutId,dateFinId,spanId, msgDateFinInferieureDebut, msgDateValide){
	var dateValides=true;
	var dateDebut= document.getElementById(dateDebutId).value;
	var dateFin= document.getElementById(dateFinId).value;
	
	if(!isNaN(dateDebut) && !isNaN(dateFin) && !(dateDebut === "") && !(dateFin==="")){
		if(parseInt(dateFin)<parseInt(dateDebut) ){
			document.getElementById(spanId).innerHTML = msgDateFinInferieureDebut;
			dateValides=false;
		}
		
	}else if((!(dateDebut === "") || !(dateFin==="")) && (isNaN(dateDebut) || isNaN(dateFin))){
		document.getElementById(spanId).innerHTML = msgDateValide;
		dateValides=false;
	}
	
	return dateValides;
}



function validationMois(moisId, spanMoisId,msgMoisValide,msgMoisNumerique){
	var valide= true;
		if(!(document.getElementById(moisId).value === "") && !isNaN(document.getElementById(moisId).value)){
			if(document.getElementById(moisId).value>12 || document.getElementById(moisId).value<1){
				
				document.getElementById(spanMoisId).innerHTML = msgMoisValide;
				valide=false;
			}
			
		}else if(!(document.getElementById(moisId).value === "") && isNaN(document.getElementById(moisId).value)){
			document.getElementById(spanMoisId).innerHTML = msgMoisNumerique;
			valide=false;
		}
		return valide;
}



function validationAnnee(anneeId,spanAnneeId,msgNumerique){
	var valide=true;
		if(!(document.getElementById(anneeId).value === "") && isNaN(document.getElementById(anneeId).value)){
			document.getElementById(spanAnneeId).innerHTML = msgNumerique ;
			valide=false;
		}
	
	return valide;
}




function validationVide(id, spanId, msg){
	var validation=true;
		if(document.getElementById(id).value === ""){
			document.getElementById(spanId).innerHTML = msg;
			validation=false;
		}
	return validation;
}




function validationCheckboxVide(classId,spanId,nbCheckbox,msg){
	var nomCheckbox=document.getElementsByClassName(classId);
	for(var i=0; i<nbCheckbox ;i++){
		if(nomCheckbox[i].checked){
			return true;
		}
	}
	document.getElementById(spanId).innerHTML = msg;
	return false;

}

