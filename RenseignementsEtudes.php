﻿<?php

require "ScriptCommun.php";
require "RenseignementsEtudesValidations.php";

?>

<!DOCTYPE html>
	<html lang="fr">
		<head>
			<title>Renseignements sur les Études</title>
			<meta charset="utf-8">
			<script src="ScriptCommun.js"></script>
			<script src="RenseignementsEtudes.js"></script>
			
			<link rel="stylesheet" type="text/css" href="style/ProgrammesDemandes.css">	
		</head>
		<body>
			<div id="page">
				<div id="entete">
                	<div class="identification" >RENSEIGNEMENTS SUR LES ÉTUDES UNIVERSITAIRES</div>
                	<div id="demande"><div id="demAd">DEMANDE D'ADMISSION</div><span id="etude">Études de premier cycle</span></div>
                	<div id="numero">3</div>
                </div>	
              	<hr class="ligne">
              	<span class="petite-police">Les relevés de notes officiels portant les sceaux de l'établissement qui les ont émis et
              	 mentionnant les notes obtenues pour les cours suivis doivent être expédiés directement au Registrariat-Admission de l'UQAM
              	 par ces établissements et ce à votre demande. Tout relevé de notes provenant d'un établissement d'enseignement hors Québec
              	 doit être accompagné d'un document émis par cet établissement permettant d'évaluer la notation utilisée. </span><br><br><br>
              	<form method="post" onsubmit="return validerForm()" >
              		<div class="partieGauche">
              			<input class="diplome-u1" type="checkbox" name="diplome" value="première diplôme case coché">
              			<span class="petite-police">Grade ou diplôme de niveau universitaire le plus récent entrepris ou complété</span>
              			<span style="color:red" class="err" id="err-diplome-u1"></span><br><br>
              	
              			<label for="nom-diplome">Nom du diplôme</label><br>
            				<input type="text" id="nom-diplome" name="nom-diplome" size="60" maxlength="60">
              				<br><span style="color:red" class="err" id="err-nom-diplome"></span><br><br>
              		
              			<label for="discipline">Discipline ou spécialisation</label><br>
            				<input type="text" id="discipline" name="discipline" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-discipline"></span><br><br>
            		
            			<label for="institution">Institution où vous avez poursuivi vos études en vue de l'obtention de ce diplôme</label><br>
            				<input type="text" id="institution" name="institution" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-institution"></span><br><br>
            		
            			<label for="pays">Pays (Si à l'extérieur du Canada)</label><br>
            				<input type="text" id="pays" name="pays" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-pays"></span><br><br>	
              	
              	
              			<input class="diplome-u2" type="checkbox" name="diplome2" value="deuxième case diplôme coché">
              			<span class="petite-police">Autre grade ou diplôme de niveau universitaire entrepris ou complété</span>
              			<span style="color:red" class="err" id="err-diplome-u2"></span><br><br>
              	
              			<label for="nom-diplome2">Nom du diplôme</label><br>
            				<input type="text" id="nom-diplome2" name="nom-diplome2" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-nom-diplome2"></span><br><br>
              		
              			<label for="discipline2">Discipline ou spécialisation</label><br>
            				<input type="text" id="discipline2" name="discipline2" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-discipline2"></span><br><br>
            		
            			<label for="institution2">Institution où vous avez poursuivi vos études en vue de l'obtention de ce diplôme</label><br>
            				<input type="text" id="institution2" name="institution2" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-institution2"></span><br><br>
            		
            			<label for="pays2">Pays (Si à l'extérieur du Canada)</label><br>
            				<input type="text" id="pays2" name="pays2" size="60" maxlength="60">
            				<br><span style="color:red" class="err" id="err-pays2"></span><br><br><br>
            			
            			<div class="identification" >RENSEIGNEMENTS SUR LES EMPLOIS</div>
            				
              		</div>
              		<div class="partieDroite">
              		
              		
              			<div>
           					<input class="obtention-u1" type="checkbox" name="obtention" value="Obtenu" onclick="UneSelection('obtention-u1',3,0)"><span> Obtenu </span> 
            				<input class="obtention-u1" type="checkbox" name="obtention" value="à obtenir" onclick="UneSelection('obtention-u1',3,0)"><span> À obtenir </span>
           					<input class="obtention-u1" type="checkbox" name="obtention" value="Non obtenu" onclick="UneSelection('obtention-u1',3,0)"><span> Ne sera pas obtenu </span>
           					<br><span style="color:red" class="err" id="err-obtention-u1"></span><br><br><br>
           					
           				</div>
						
              			
              			<div>Période de fréquentation</div>
							<input type="text" id="annee-debut" name="annee-debut" size="2" maxlength="2"><span> De (année)</span>
							<input type="text" id="annee-fin" name="annee-fin" size="2" maxlength="2"><span> à (année)</span>
							<span style="color:red" class="err" id="err-annee"></span>
							<br><span style="color:red" class="err" id="err-debut-freq1"></span>
							<br><span style="color:red" class="err" id="err-fin-freq1"></span>
							<br><br><br>
              				
              		
              			<div>Date d'obtention</div>
							<input type="text" id="mois-obtention" name="mois-obtention" size="2" maxlength="2"><span> Mois </span>
							<input type="text" id="annee-obtention" name="annee-obtention" size="2" maxlength="2"><span> Année </span>
							<br><span style="color:red" class="err" id="err-mois-obt1"></span>
							<br><span style="color:red" class="err" id="err-annee-obt1"></span><br><br><br>  
							
						<input type="text" id="nb-credit" name="nb-credit" size="3" maxlength="3"><span> Nombre de crédits complétés </span>
						<br><span style="color:red" class="err" id="err-nb-credit1"></span><br><br><br>         				          				
              		
              		
              			<div>
           					<input class="obtention-u2" type="checkbox" name="obtention2" value="Obtenu" onclick="UneSelection('obtention-u2',3,1)"><span> Obtenu </span> 
            				<input class="obtention-u2" type="checkbox" name="obtention2" value="À obtenir" onclick="UneSelection('obtention-u2',3,1)"><span> À obtenir </span>
           					<input class="obtention-u2" type="checkbox" name="obtention2" value="Non obtenu" onclick="UneSelection('obtention-u2',3,1)"><span> Ne sera pas obtenu </span>
           					<br><span style="color:red" class="err" id="err-obtention-u2"></span><br><br><br>
           				</div>
						
              			
              			<div>Période de fréquentation</div>
							<input type="text" id="annee-debut2" name="annee-debut2" size="2" maxlength="2"><span> De (année)</span>
							<input type="text" id="annee-fin2" name="annee-fin2" size="2" maxlength="2"><span> à (année)</span>
              				<span style="color:red" class="err" id="err-annee2"></span>
              				<br><span style="color:red" class="err" id="err-debut-freq2"></span>
              				<br><span style="color:red" class="err" id="err-fin-freq2"></span>
              				<br><br><br>
              				
              			<div>Date d'obtention</div>
							<input type="text" id="mois-obtention2" name="mois-obtention2" size="2" maxlength="2"><span> Mois </span>
							<input type="text" id="annee-obtention2" name="annee-obtention2" size="2" maxlength="2"><span> Année </span>
							<br><span style="color:red" class="err" id="err-mois-obt2"></span>
							<br><span style="color:red" class="err" id="err-annee-obt2"></span><br><br><br>  
							
						<input type="text" id="nb-credit2" name="nb-credit2" size="3" maxlength="3"><span> Nombre de crédits complétés </span>
						<br><span style="color:red" class="err" id="err-nb-credit2"></span><br><br><br>         				          				
              			
              		
              		</div>
              			
              	<hr class="ligne">
            	<p class="petite-police">Il est essentiel de joindre les attestations des employeurs ou des responsables.
            	 En plus de confirmer la durée et la nature des emplois occupés, les attestations doivent contenir une
            	 brève description des fonctions ou des tâches accomplies. Un curriculum vitae ou un contrat de travail
            	  ne peut d'aucune manière être considéré comme une preuve d'emplois. L'absence d'attestation(s) peut entrainer le refus d'admission.
            	</p>
            	<div class="partieGauche">
            	
            	
            		<label for="nom-employeur">Nom de l'employeur</label><br>
            			<input type="text" id="nom-employeur" name="nom-employeur" size="60" maxlength="60">
            			<br><span style="color:red" class="err" id="err-nom-employeur"></span><br><br>
            		
            		<label for="fonction">Fonction occupée</label><br>
            			<input type="text" id="fonction" name="fonction" size="60" maxlength="60">
            			<br><span style="color:red" class="err" id="err-fonction"></span><br><br>
            			
            		<input class="genre-emploi" type="checkbox" name="genre-emploi" value="Emploi rénuméré" onclick="UneSelection('genre-emploi',3,2)">Emploi rénuméré
            		<input class="genre-emploi" type="checkbox" name="genre-emploi" value="Stage" onclick="UneSelection('genre-emploi',3,2)">Stage
            		<input class="genre-emploi" type="checkbox" name="genre-emploi" value="Bénévolat" onclick="UneSelection('genre-emploi',3,2)">Bénévolat
            		<br><span style="color:red" class="err" id="err-genre-emploi"></span><br>
            	
           			<input class="temps-emploi" type="checkbox" name="temps-emploi" value="Temps-complet" onclick="UneSelection('temps-emploi',2,3)">Temps complet
           			<input class="temps-emploi" type="checkbox" name="temps-emploi" value="Temps-partiel" onclick="UneSelection('temps-emploi',2,3)">Temps partiel
           			<br><span style="color:red" class="err" id="err-temps-emploi"></span><br><br>
           					
           				
           		
           			<label for="nom-employeur2">Nom de l'employeur</label><br>
            			<input type="text" id="nom-employeur2" name="nom-employeur2" size="60" maxlength="60">
            			<br><span style="color:red" class="err" id="err-nom-employeur2"></span><br><br>
            			
            			
            		<label for="fonction2">Fonction occupée</label><br>
            			<input type="text" id="fonction2" name="fonction2" size="60" maxlength="60">
            			<br><span style="color:red" class="err" id="err-fonction2"></span><br><br>
            			
            			
            		<input class="genre-emploi2" type="checkbox" name="genre-emploi2" value="Emploi rénuméré" onclick="UneSelection('genre-emploi2',3,4)">Emploi rénuméré
            		<input class="genre-emploi2" type="checkbox" name="genre-emploi2" value="Stage" onclick="UneSelection('genre-emploi2',3,4)">Stage
            		<input class="genre-emploi2" type="checkbox" name="genre-emploi2" value="Bénévolat" onclick="UneSelection('genre-emploi2',3,4)">Bénévolat
            		<br><span style="color:red" class="err" id="err-genre-emploi2"></span><br>
            		
            	
           			<input class="temps-emploi2" type="checkbox" name="temps-emploi2" value="Temps-complet" onclick="UneSelection('temps-emploi2',2,5)">Temps complet
           			<input class="temps-emploi2" type="checkbox" name="temps-emploi2" value="Temps-partiel" onclick="UneSelection('temps-emploi2',2,5)">Temps partiel
           			<br><span style="color:red" class="err" id="err-temps-emploi2"></span><br><br>
           				
            		
            		<label for="nom-employeur3">Nom de l'employeur</label><br>
            			<input type="text" id="nom-employeur3" name="nom-employeur3" size="60" maxlength="60">
						<br><span style="color:red" class="err" id="err-nom-employeur3"></span><br><br>
						            		
            		<label for="fonction3">Fonction occupée</label><br>
            			<input type="text" id="fonction3" name="fonction3" size="60" maxlength="60">
            			<br><span style="color:red" class="err" id="err-fonction3"></span><br><br>
            			
            			
            		<input class="genre-emploi3" type="checkbox" name="genre-emploi3" value="Emploi rénuméré" onclick="UneSelection('genre-emploi3',3,6)">Emploi rénuméré
            		<input class="genre-emploi3" type="checkbox" name="genre-emploi3" value="Stage" onclick="UneSelection('genre-emploi3',3,6)">Stage
            		<input class="genre-emploi3" type="checkbox" name="genre-emploi3" value="Bénévolat" onclick="UneSelection('genre-emploi3',3,6)">Bénévolat
            		<br><span style="color:red" class="err" id="err-genre-emploi3"></span><br>
            		
            	
           			<input class="temps-emploi3" type="checkbox" name="temps-emploi3" value="Temps-complet" onclick="UneSelection('temps-emploi3',2,7)">Temps complet
           			<input class="temps-emploi3" type="checkbox" name="temps-emploi3" value="Temps-partiel" onclick="UneSelection('temps-emploi3',2,7)">Temps partiel
           		    <br><span style="color:red" class="err" id="err-temps-emploi3"></span><br><br>
           		
            		
            	</div>
            	<div class="partieDroite">
            	
            		<div>Durée de l'emploi</div>
            			<span>De</span>
						<input type="text" id="mois-debut-emploi" name="mois-debut-emploi" size="2" maxlength="2"><span> Mois</span>
						<input type="text" id="annee-debut-emploi" name="annee-debut-emploi" size="2" maxlength="2"><span> Année</span><br><br>           				
           				<span> à&nbsp;&nbsp; </span>
            			<input type="text" id="mois-fin-emploi" name="mois-fin-emploi" size="2" maxlength="2"><span> Mois</span>
						<input type="text" id="annee-fin-emploi" name="annee-fin-emploi" size="2" maxlength="2"><span> Année</span>           				
           			
           				<br><span style="color:red" class="err" id="err-mois-debut"></span>	
            			<br><span style="color:red" class="err" id="err-annee-debut"></span>
            			<br><span style="color:red" class="err" id="err-mois-fin"></span>
            			<br><span style="color:red" class="err" id="err-annee-fin"></span><br>
            		
            		<div class="emploi">Durée de l'emploi</div>
            			<span>De</span>
						<input type="text" id="mois-debut-emploi2" name="mois-debut-emploi2" size="2" maxlength="2"><span> Mois</span>
						<input type="text" id="annee-debut-emploi2" name="annee-debut-emploi2" size="2" maxlength="2"><span> Année</span><br><br>           				
           				<span> à&nbsp;&nbsp; </span>
            			<input type="text" id="mois-fin-emploi2" name="mois-fin-emploi2" size="2" maxlength="2"><span> Mois</span>
						<input type="text" id="annee-fin-emploi2" name="annee-fin-emploi2" size="2" maxlength="2"><span> Année</span>         				
					
						<br><span style="color:red" class="err" id="err-mois-debut2"></span>	
            			<br><span style="color:red" class="err" id="err-annee-debut2"></span>
            			<br><span style="color:red" class="err" id="err-mois-fin2"></span>
            			<br><span style="color:red" class="err" id="err-annee-fin2"></span><br>
						           		
           		
           			<div class="emploi">Durée de l'emploi</div>
            			<span>De</span>
						<input type="text" id="mois-debut-emploi3" name="mois-debut-emploi3" size="2" maxlength="2"><span> Mois</span>
						<input type="text" id="annee-debut-emploi3" name="annee-debut-emploi3" size="2" maxlength="2"><span> Année</span><br><br>           				
           				<span> à&nbsp;&nbsp; </span>
            			<input type="text" id="mois-fin-emploi3" name="mois-fin-emploi3" size="2" maxlength="2"><span> Mois</span>
						<input type="text" id="annee-fin-emploi3" name="annee-fin-emploi3" size="2" maxlength="2"><span> Année</span>          				
           			
           				<br><span style="color:red" class="err" id="err-mois-debut3"></span>	
            			<br><span style="color:red" class="err" id="err-annee-debut3"></span>
            			<br><span style="color:red" class="err" id="err-mois-fin3"></span>
            			<br><span style="color:red" class="err" id="err-annee-fin3"></span><br>
           			
           			<input class="page-suivante" type="submit" value="Page suivante" name= "submit"/>	
            	</div>
            	
            	
            		
              </form>		
			</div>
		</body>
	</html>