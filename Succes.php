﻿<?php
session_start();

function afficherDonnees(){
	$derniereLigne=" ";
	$fichierFormulaire= $_SESSION['fichier_code_permanent'];
	$fichier=file($fichierFormulaire);
	foreach ($fichier as $ligne){
		$ligne=(strlen($ligne)!==strlen(utf8_decode($ligne)))? $ligne : utf8_encode($ligne);
		echo $derniereLigne=str_replace("\n","<br>",$ligne);
	}
	
	
}

?>

<!DOCTYPE html>
	<html lang="fr">
		<head>
			<title>Confirmation</title>
			<meta charset="utf-8">
			<link rel="stylesheet" type="text/css" href="style/ProgrammesDemandes.css">	
				
		</head>
		<body>
		   <div id="page">
					<div id="entete">
                		<div class="identification" >SOUMISSION TERMINÉ</div>
                		<div id="demande"><div id="demAd">DEMANDE D'ADMISSION</div><span id="etude">Études de premier cycle</span></div>
                		<div id="numero">5</div>
                	</div>
            
            <hr class="ligne">
            <?php afficherDonnees();?>
            
            </div>
		</body>
	</html>
            