﻿

function validerForm(){

	effacerErreurs();
	var validationDiplome1 =  validationDiplome("diplome-u1","nom-diplome","err-nom-diplome",
		"discipline","err-discipline","institution","err-institution","pays","err-pays","obtention-u1",
		"err-obtention-u1","annee-debut","err-debut-freq1","annee-fin","err-fin-freq1","mois-obtention",
		"err-mois-obt1","annee-obtention","err-annee-obt1","nb-credit","err-nb-credit1","err-annee","err-diplome-u1");

	var validationDiplome2 = validationDiplome("diplome-u2","nom-diplome2","err-nom-diplome2",
			"discipline2","err-discipline2","institution2","err-institution2","pays2","err-pays2",
			"obtention-u2","err-obtention-u2","annee-debut2","err-debut-freq2","annee-fin2","err-fin-freq2",
			"mois-obtention2","err-mois-obt2","annee-obtention2","err-annee-obt2","nb-credit2","err-nb-credit2",
			"err-annee2","err-diplome-u2");
	
	var validationEmploi1 = validationEmploi("nom-employeur","fonction","err-fonction","mois-debut-emploi",
			"err-mois-debut","annee-debut-emploi","err-annee-debut","mois-fin-emploi","err-mois-fin",
			"annee-fin-emploi","err-annee-fin","genre-emploi","err-genre-emploi","temps-emploi","err-temps-emploi","err-nom-employeur");
	
	var validationEmploi2 = validationEmploi("nom-employeur2","fonction2","err-fonction2","mois-debut-emploi2",
			"err-mois-debut2","annee-debut-emploi2","err-annee-debut2","mois-fin-emploi2","err-mois-fin2",
			"annee-fin-emploi2","err-annee-fin2","genre-emploi2","err-genre-emploi2","temps-emploi2","err-temps-emploi2","err-nom-employeur2");
	
	var validationEmploi3 = validationEmploi("nom-employeur3","fonction3","err-fonction3","mois-debut-emploi3",
			"err-mois-debut3","annee-debut-emploi3","err-annee-debut3","mois-fin-emploi3","err-mois-fin3",
			"annee-fin-emploi3","err-annee-fin3","genre-emploi3","err-genre-emploi3","temps-emploi3","err-temps-emploi3","err-nom-employeur3");
	
	return validationDiplome1 && validationDiplome2
			&& validationEmploi1 && validationEmploi2
			&& validationEmploi3; 
}








function validationDateDureeEmploi(moisDebutId,moisDebutSpanId,moisFinId,
		anneeDebutId,anneeFinId,anneeDebutValide,anneeFinValide,
		moisDebutValide,moisFinValide,msgDateFinSupperieurDebut){
	
	var moisDebut= document.getElementById(moisDebutId).value;
	var moisFin= document.getElementById(moisFinId).value;
	var anneeDebut=document.getElementById(anneeDebutId).value;
	var anneeFin=document.getElementById(anneeFinId).value;
	var valide=true;
		if(!(moisDebut==="") && !(moisFin==="") && !(anneeDebut==="") && !(anneeFin==="") 
				&& anneeDebutValide && anneeFinValide && moisDebutValide && moisFinValide){
			valide = compareDate(moisDebut,moisFin,anneeDebut,anneeFin,
					moisDebutSpanId,msgDateFinSupperieurDebut);
		}
	return valide;
}



function compareDate(moisDebut,moisFin,anneeDebut,anneeFin,spanId,msgDateFinSupperieurDebut){
	var valide=true;
		if(parseInt(anneeDebut)>parseInt(anneeFin) 
				|| (parseInt(anneeDebut)===parseInt(anneeFin) 
				&& parseInt(moisDebut)>parseInt(moisFin))){
			
			document.getElementById(spanId).innerHTML = msgDateFinSupperieurDebut;
			valide= false;
		}
	return valide;
}




function validationDiplome(checkboxClassId,diplomeId,diplomeSpanId,disciplineId,
		disciplineSpanId,institutionId,institutionSpanId,paysId,paysSpanId,obtentionClassId,
		obtentionSpanId,debutId,debutSpanId,finId,finSpanId,moisObtId,moisObtSpanId,
		anneeObtId,anneeObtSpanId,nbCreditId,nbCreditSpanId,erreurAnneeSpanId,checkboxSpanId){
	var diplomeValide=true; 
	var disciplineValide=true; 
	var institutionValide=true;
	var paysValide=true;
	var obtentionValide=true;
	var debutValide=true; 
	var finValide=true; 
	var moisObtValide=true; 
	var anneeObtValide=true;
	var nbCreditValide =true;
	var validationAnneeFreq=true;
	var checkboxValide= true;
		if(validationCheckboxVide(checkboxClassId,checkboxSpanId,1,"") 
			|| validationVide(diplomeId, diplomeSpanId,"")
			|| validationVide(disciplineId, disciplineSpanId,"")
			|| validationVide(institutionId, institutionSpanId,"")
			|| validationVide(paysId,paysSpanId,"")
			|| validationCheckboxVide(obtentionClassId,obtentionSpanId,3,"")
			|| validationVide(debutId,debutSpanId,"")
			|| validationVide(finId,finSpanId,"")
			|| validationVide(moisObtId,moisObtSpanId,"")
			|| validationVide(anneeObtId,anneeObtSpanId,"")
			|| validationVide(nbCreditId,nbCreditSpanId,"")){
		
			checkboxValide=validationCheckboxVide(checkboxClassId,checkboxSpanId,1,
					"*Vous devez cocher la case");
			diplomeValide=validationVide(diplomeId, diplomeSpanId,
					"*Vous devez entrer le Nom du diplôme");
			disciplineValide=validationVide(disciplineId, disciplineSpanId,
					"*Vous devez entrer la Discipline ou spécialisation");
			institutionValide=validationVide(institutionId, institutionSpanId,
					"*Vous devez entrer entrer l'institution où vous avez poursuivi vos études");
			paysValide=validationVide(paysId,paysSpanId,
					"*Vous devez entrer le nom du pays où vous avez poursuivi vos études");
			obtentionValide=validationCheckboxVide(obtentionClassId,obtentionSpanId,3,
					"*Vous devez cocher Obtenu ou À obtenir ou Ne sera pas obtenu");
			debutValide=validationVide(debutId,debutSpanId,
					"*Vous devez entrer une année de début ");
			finValide=validationVide(finId,finSpanId,
					"*Vous devez entrer une année de fin ");
			moisObtValide=validationVide(moisObtId,moisObtSpanId,
					"*Vous devez entrer le mois d'obtention ");
			moisObtValide2=validationMois(moisObtId, moisObtSpanId,
					"*le mois doit etre entre 1 et 12",
					"*le mois doit etre numerique");
			anneeObtValide=validationVide(anneeObtId,anneeObtSpanId,
					"*Vous devez entrer l'année d'obtention ");
			anneeObtValide2=validationAnnee(anneeObtId,anneeObtSpanId,
					"*l'année doit etre numerique")
			nbCreditValide=validationVide(nbCreditId,nbCreditSpanId,
					"*Vous devez entrer le nombre de crédits complétés ");
			validationAnneeFreq = validationDates(debutId,finId,erreurAnneeSpanId,
					"*L'annee de fin doit être suppérieure ou egale à l'annee de début",
					"*Entrer une annee valide");
		}
	
	return checkboxValide && diplomeValide && disciplineValide 
			&& institutionValide && paysValide
			&& obtentionValide && debutValide 
			&& finValide && moisObtValide && validationAnneeFreq
			&& anneeObtValide && nbCreditValide;
	
}
function validationEmploi(emploiId,fonctionId,fonctionSpanId,moisDebutId,
		moisDebutSpanId,anneeDebutId,anneeDebutSpanId,moisFinId,moisFinSpanId,
		anneeFinId,anneeFinSpanId,genreEmploiClassId,genreEmploiSpanId,
		tempsClassId,tempsSpanId,employeurSpanId){
		var fonctionValid=true; 
		var moisDebutValid=true; 
		var anneeDebutValid=true; 
		var moisFinValid=true;
		var anneeFinValid=true; 
		var genreValid=true;
		var tempsEmploivalid=true;
		var moisDebutValide=true;
		var moisFinValide=true;
		var anneeDebutValide=true;
		var anneeFinValide=true;
		var dateValide=true;
		var nomEmployeurValide=true;
			if(validationVide(emploiId, employeurSpanId,"")
				|| validationVide(fonctionId, fonctionSpanId,"")
				|| validationVide(moisDebutId, moisDebutSpanId,"")
				|| validationVide(anneeDebutId, anneeDebutSpanId,"")
				|| validationVide(moisFinId, moisFinSpanId,"")
				|| validationVide(anneeFinId, anneeFinSpanId,"")
				|| validationCheckboxVide(genreEmploiClassId,genreEmploiSpanId,3,"")
				|| validationCheckboxVide(tempsClassId,tempsSpanId,2,"")){
		
		nomEmployeurValide=validationVide(emploiId, employeurSpanId,
				"*Vous devez entrer le Nom de l'employeur ");
		
		fonctionValid=validationVide(fonctionId, fonctionSpanId,
				"*Vous devez entrer la fonction occupée");
			
		moisDebutValid=validationVide(moisDebutId, moisDebutSpanId,
				"*Vous devez entrer le mois du début du champ 'Durée de l'emploi'");
		
		anneeDebutValid=validationVide(anneeDebutId, anneeDebutSpanId,
				"*Vous devez entrer l'année de début du champ 'Durée de l'emploi'");
		moisFinValid=validationVide(moisFinId, moisFinSpanId,
				"*Vous devez entrer le mois de fin du champ 'Durée de l'emploi'");
		anneeFinValid=validationVide(anneeFinId, anneeFinSpanId,
				"*Vous devez entrer l'année de fin du champ 'Durée de l'emploi'");
		
		genreValid=validationCheckboxVide(genreEmploiClassId,genreEmploiSpanId,3,
				"*Vous devez indiquer le genre d'emploi occupée");
		
		tempsEmploivalid=validationCheckboxVide(tempsClassId,tempsSpanId,2,
				"*Vous devez cocher si c'est Temps complet ou Temps partiel");
		moisDebutValide=validationMois(moisDebutId, moisDebutSpanId,
				"*le mois du debut doit etre entre 1 et 12",
				"*le mois du debut doit etre numerique");
		moisFinValide=validationMois(moisFinId, moisFinSpanId,
				"*le mois de la fin doit etre entre 1 et 12",
				"*le mois de la fin doit etre numerique");
		anneeDebutValide=validationAnnee(anneeDebutId,anneeDebutSpanId,
						"*l'année du debut doit etre numerique");
		anneeFinValide=validationAnnee(anneeFinId,anneeFinSpanId,
						"*l'année de la fin doit etre numerique");
		dateValide= validationDateDureeEmploi(moisDebutId,moisDebutSpanId,moisFinId,
				anneeDebutId,anneeFinId,anneeDebutValide,anneeFinValide,moisDebutValide,moisFinValide,
				"*La date de début ne peut être suppérieur à la date de fin");

	}
	return fonctionValid && moisDebutValid 
		 && anneeDebutValid && moisFinValid
		 && anneeFinValid && genreValid
		 && tempsEmploivalid && moisDebutValide
		 && moisFinValide && anneeDebutValide
		 && anneeFinValide && dateValide && nomEmployeurValide;
}





