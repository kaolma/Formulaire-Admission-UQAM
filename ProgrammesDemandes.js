﻿function validerFormProgrammesDemandes(){
	
	
	effacerErreurs();

	var champsObligatoireValide=validationDesChampsObligatoires();
	
	var champsNonObligatoires1=validationChoixNonObligatoire("deuxieme-choix" ,"err-deuxieme-choix" ,
			"code-deuxieme-choix" ,"err-code-deuxieme-choix" ,
			"choix2", "err-choix2","temps-choix2","err-temps-choix2");
	var champsNonObligatoires2=validationChoixNonObligatoire("troisieme-choix" ,"err-troisieme-choix" ,
			"code-troisieme-choix" ,"err-code-troisieme-choix" ,
			"choix3", "err-choix3","temps-choix3","err-temps-choix3");
	var secondaireQuebec=validationSecondaireQc();
	var diplomeCollegial=validationChampsDiplomeCollegiale();
	var diplomeCollegialQuebec=validationChampsDiplomeCollegialQc();
	
	return champsObligatoireValide && champsNonObligatoires1
		&& champsNonObligatoires2 && secondaireQuebec
		&& diplomeCollegial && diplomeCollegialQuebec ;
	
}


function validationBoutonRadio(){
	var valeur="";
	var valeurExiste=true;
	
	try{
		valeur=document.querySelector('input[name=trimestre]:checked').value;
	}
	catch(err){ 
		document.getElementById("err-trimestre").innerHTML ="*Le champ trimestre" +
				" est obligatoire";
		valeurExiste=false;
	}
	return valeurExiste;

}


function validationAnneeCourante(){
	var today=new Date();
	var anneeCourante = today.getFullYear();
	var anneeForm = document.getElementById("annee").value;
	var anneeValide=true;
		if(anneeForm===""){
			document.getElementById("err-annee").innerHTML = " *Le champ Année" +
					" est obligatoire";
			anneeValide=false
		}
		else if(  anneeForm<anneeCourante && anneeForm!="" ){
			document.getElementById("err-annee").innerHTML = " *Le champ Année" +
					" est invalide, il doit être supérieur ou égale à l'année courante";
			anneeValide=false;
		}
	return anneeValide;
}

function validationDesChampsObligatoires(){
	var trimestreBoutonRadio=validationBoutonRadio();
	var anneeTrimestre=validationAnneeCourante();
	var premierChoix=validationVide("premier-choix", "err-premier-choix",
					"*Le champ 'Premier choix' est obligatoire");
	var codePremierChoix=validationVide("code-premier-choix", "err-code-premier-choix",
					"*Le champ 'Code' est obligatoire");
	var niveauPremierChoix=validationCheckboxVide("choix1","err-choix1",5,
					"*Le champ pour le niveau d'étude est obligatoire");
	var tempsPremierChoix=validationCheckboxVide("temps-choix1","err-temps-choix1",2,
					"*Vous devez choisir un champ parmis Temps-partiel ou Temps-complet");
	return trimestreBoutonRadio && anneeTrimestre
		   && premierChoix && codePremierChoix
		   && tempsPremierChoix && tempsPremierChoix;
	
}




function validationChoixNonObligatoire(choixId ,choixSpanId ,codeId ,codeSpanId ,
		niveauClassId, niveauSpanId,tempsClassId,tempsSpanId){
	var choix=true;
	var codeChoix=true;
	var niveauChoix=true;
	var tempsChoix=true;
	if(	  validationVide(choixId, choixSpanId, "")
	   || validationVide(codeId, codeSpanId, "")
	   || validationCheckboxVide(niveauClassId,niveauSpanId,5,"")
	   || validationCheckboxVide(tempsClassId,tempsSpanId,2,"")){
		

		choix=validationVide(choixId, choixSpanId,
				"*Vous devez entrer votre choix");
		codeChoix=validationVide(codeId, codeSpanId,
				"*Vous devez entrer le code du choix entrer");
		niveauChoix= validationCheckboxVide(niveauClassId,niveauSpanId,5,
				"*Vous devez entrer le niveau du choix entrer");
		tempsChoix= validationCheckboxVide(tempsClassId,tempsSpanId,2,
				"*Vous devez choisir un champ parmis Temps-partiel ou Temps-complet");
		
		
	}
	return choix && niveauChoix
		   codeChoix && niveauChoix
		   tempsChoix;
}



function validationSecondaireQc(){
	var nom=true;
	var periodeFreq=true;
	var anneeDebut=true;
	var anneeFin= true;
	if(validationVide("etude-secondaire-quebec","err-etude-secondaire-quebec","")
		|| validationVide("annee-debut-quebec","err-annee-d-qc","")
		|| validationVide("annee-fin-quebec","err-annee-f-qc","")
		){
		
		nom=validationVide("etude-secondaire-quebec", 
							"err-etude-secondaire-quebec",
							"*vous devez entrer 'Dernière année du secondaire complétée au québec'" );
		periodeFreq=validationDates("annee-debut-quebec","annee-fin-quebec","err-annee-debut-qc", 
							"*L'annee de fin doit être suppérieure ou egale à l'annee de début",
							"*Entrer une annee valide");
		anneeDebut=validationVide("annee-debut-quebec","err-annee-d-qc",
							"*Vous devez entrer une année de début ")
		anneeFin=validationVide("annee-fin-quebec","err-annee-f-qc",
							"*Vous devez entrer une année de fin ")

	}
	return nom && periodeFreq && anneeDebut && anneeFin;
		

}



function validationChampsDiplomeCollegiale(){
	var diplomeCheckbox=true;
	var nomDiplome=true;
	var discipline=true;
	var institution=true;
	var pays=true;
	var anneeDebut=true;
	var anneeFin=true;
	var periodeFrequentation=true;
	var mois=true;
	var annee=true;
	var anneeValide=true;
	var moisValide=true;
		
	if(validationCheckboxVide("diplome-hors","err-diplome-hors",1,"")
			|| validationVide("nom-diplome", "err-nom-diplome", "")
			|| validationVide("discipline", "err-discipline", "")
			|| validationVide("institution", "err-institution", "")
			|| validationVide("pays", "err-pays", "")
			|| validationVide("annee-debut", "err-annee-d", "")
			|| validationVide("annee-fin", "err-annee-f", "")
			|| validationVide("mois-obtention", "err-mois-obt", "")
			|| validationVide("annee-obtention", "err-annee-obt", "")){
		
		diplomeCheckbox=validationCheckboxVide("diplome-hors","err-diplome-hors",1,
						"*Vous devez cocher la case du diplome");
		nomDiplome=validationVide("nom-diplome", "err-nom-diplome",
						"*Vous devez entrer le Nom du diplôme");
		discipline=validationVide("discipline", "err-discipline", 
						"*Vous devez entrer la Discipline ou spécialisation");
		institution=validationVide("institution", "err-institution", 
						"*Vous devez entrer entrer l'institution où vous avez poursuivi vos études");
		pays=validationVide("pays", "err-pays", 
						"*Vous devez entrer le nom du pays où vous avez poursuivi vos études");
		anneeDebut=validationVide("annee-debut", "err-annee-d", 
						"*Vous devez entrer une année de début ");
		anneeFin=validationVide("annee-fin", "err-annee-f", 
						"*Vous devez entrer une année de fin ");
		periodeFrequentation=validationDates("annee-debut","annee-fin","err-annee-debut",
						"*L'annee de fin doit être suppérieure ou egale à l'annee de début", 
						"*Entrer une annee valide");
		mois=validationVide("mois-obtention", "err-mois-obt",
						"*Vous devez entrer le mois d'obtention ");
		annee=validationVide("annee-obtention", "err-annee-obt", 
						"*Vous devez entrer l'année d'obtention ");
		moisValide=validationMois("mois-obtention", "err-mois-obt",
						"*le mois doit etre entre 1 et 12",
						"*le mois doit etre numerique");
		anneeValide=validationAnnee("annee-obtention","err-annee-obt",
						"*l'année doit etre numerique");
		
	}
	
	return diplomeCheckbox && nomDiplome
		&& discipline && institution 
		&& pays && anneeDebut 
		&& anneeFin && periodeFrequentation 
		&& mois && annee && anneeValide;

}

function validationChampsDiplomeCollegialQc(){
	var diplomeCheckbox=true;
	var nomDiplome=true;
	var discipline=true;
	var institution=true;
	var anneeDebut=true;
	var anneeFin=true;
	var periodeFrequentation=true;
	var mois=true;
	var annee=true;
	var anneeValide=true;
	var moisValide=true;
	var obtenuCheckbox=true;
	
		
	if(validationCheckboxVide("dec-ou-autre","err-dec-ou-autre",2,"")
			|| validationVide("nom-diplome-dec", "err-nom-dec", "")
			|| validationVide("discipline-dec", "err-discipline-dec", "")
			|| validationVide("institution-dec", "err-institution-dec", "")
			|| validationVide("annee-debut-dec", "err-annee-d-dec", "")
			|| validationVide("annee-fin-dec", "err-annee-f-dec", "")
			|| validationVide("mois-obtention-dec", "err-mois-obt-dec", "")
			|| validationVide("annee-obtention-dec", "err-annee-obt-dec", "")
			|| validationCheckboxVide("obtention-dec","err-obtention-dec",3,"")){
		

		diplomeCheckbox=validationCheckboxVide("dec-ou-autre","err-dec-ou-autre",2,
						"*Vous devez cocher la case du diplome");
		nomDiplome=validationVide("nom-diplome-dec", "err-nom-dec",
						"*Vous devez entrer le Nom du diplôme");
		discipline=validationVide("discipline-dec", "err-discipline-dec", 
						"*Vous devez entrer la Discipline ou spécialisation");
		institution=validationVide("institution-dec", "err-institution-dec", 
						"*Vous devez entrer entrer l'institution où vous avez poursuivi vos études");
		anneeDebut=validationVide("annee-debut-dec", "err-annee-d-dec", 
						"*Vous devez entrer une année de début ");
		anneeFin=validationVide("annee-fin-dec", "err-annee-f-dec", 
						"*Vous devez entrer une année de fin ");
		periodeFrequentation=validationDates("annee-debut-dec","annee-fin-dec","err-annee-debut-dec",
						"*L'annee de fin doit être suppérieure ou egale à l'annee de début", 
						"*Entrer une annee valide");
		moisValide=validationVide("mois-obtention-dec", "err-mois-obt-dec",
						"*Vous devez entrer le mois d'obtention ");
		annee=validationVide("annee-obtention-dec", "err-annee-obt-dec", 
						"*Vous devez entrer l'année d'obtention ");
		mois=validationMois("mois-obtention-dec", "err-mois-obt-dec",
						"*le mois doit etre entre 1 et 12",
						"*le mois doit etre numerique");
		anneeValide=validationAnnee("annee-obtention-dec","err-annee-obt-dec",
						"*l'année doit etre numerique");
		obtenuCheckbox=validationCheckboxVide("obtention-dec","err-obtention-dec",3,
		"*Vous devez cocher Obtenu ou À obtenir ou Ne sera pas obtenu");
		
	}
	
	return obtenuCheckbox && diplomeCheckbox && nomDiplome
		&& discipline && institution 
		&& anneeDebut 
		&& anneeFin && periodeFrequentation 
		&& mois && annee && anneeValide;
}

