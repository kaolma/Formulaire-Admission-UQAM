<?php
session_start();
    function redirection(){
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(verificationIsset()){
                if(verificationDonnees()){
                    http_response_code(400);
                    exit;
                }else{
                    ecritureFichier();
                    header("Location: ProgrammesDemandes.php", true, 303);
                    exit;
                }
            }
        }
    } 
    function verificationIsset(){

    return  isset($_POST['nom'])|| isset($_POST['prenom']) || isset($_POST['code-permanent1'])  
            || isset($_POST['code-permanent2']) || isset($_POST['citoyennete']) || isset($_POST['autre-citoyennete']) || isset($_POST['lieu-naissance'])
            || isset($_POST['nom-pere']) || isset($_POST['prenom-pere']) || isset($_POST['nom-mere'])
            || isset($_POST['prenom-mere']) || isset($_POST['tel-dom']) || isset($_POST['tel-cel']) 
            || isset($_POST['tel-trv']) || isset($_POST['post']) || isset($_POST['courriel'])
            || isset($_POST['adresse']) || isset($_POST['num-appt1']) || isset($_POST['mun']) 
            || isset($_POST['code-postal1']) || isset($_POST['adresse2']) || isset($_POST['num-appt2']) 
            || isset($_POST['mun2']) || isset($_POST['code-postal2']) || isset($_POST['date-naissance']) 
            || isset($_POST['sexe']) || isset($_POST['nas']) || isset($_POST['statut'])  
            || isset($_POST['langue-usage']) || isset($_POST['champ-autre-usage']) || isset($_POST['langue-maternelle']) 
            || isset($_POST['champ-autre-maternelle']);

    }
        
    function ecritureFichier(){
	$nomFichier = $_POST['code-permanent2'].".txt";
	$_SESSION['fichier_code_permanent']=$nomFichier;
	$fichierFormulaire = fopen($nomFichier, 'a+');
	if(!$fichierFormulaire){
		http_response_code(400);
                exit;
	}
	else{
		
		$contenu= "\n"."Nom de famille à la naissance: " . $_POST['nom']."\n"
                              ."Prénom usuel: " . $_POST['prenom']."\n"
                              ."Code permanent UQÀM: " . $_POST['code-permanent1']."\n"
                              ."Code permanent du ministère de l'éducation et de l'enseignement supérieur: " . $_POST['code-permanent2']."\n"  
                              ."Citoyenneté: " . $_POST['citoyennete']."\n"
                              ."Autre Citoyenneté: " . $_POST['autre-citoyennete']."\n"
                              ."Lieu de naissance: " . $_POST['lieu-naissance']."\n"
                              ."Nom de famille du père à la naissance: " . $_POST['nom-pere']."\n"
                              ."Prénom usuel du père: " . $_POST['prenom-pere']."\n"
                              ."Nom de famille de la mère à la naissance: " . $_POST['nom-mere']."\n"
                              ."Prénom usuel de la mère: " . $_POST['prenom-mere']."\n"
                              ."Téléphone à domicile: " . $_POST['tel-dom']."\n"
                              ."Cellulaire: " . $_POST['tel-cel']."\n"
                              ."Téléphone au travail: " . $_POST['tel-trv']."            Numéro de poste: " . $_POST['poste']."\n"
                              ."Courriel: " . $_POST['courriel']."\n"
                              ."Adresse de correspondance: " . $_POST['adresse']."        N° d'appartement : " . $_POST['num-appt1']."\n"
                              ."Municipalité: " . $_POST['mun']."          Code postal : " . $_POST['code-postal1']."\n"
                              ."Adresse de résidence actuelle: " . $_POST['adresse2']."N° d'appartement : " . $_POST['num-appt2']."\n"
                              ."Municipalité: " . $_POST['mun2']."         Code postal : " . $_POST['code-postal2']."\n"
                              ."Date de naissance: " . $_POST['date-naissance']."\n"
                              ."Sexe: " . $_POST['sexe']."\n"
                              ."N° d'assurance sociale: " . $_POST['nas']."\n"
                              ."Statut au Canada: " . $_POST['statut']."\n"
                              ."Langue d'usage: " . $_POST['langue-usage']."\n"
                              ."Autre langue d'usage: " . $_POST['champ-autre-usage']."\n"
                              ."Langue maternelle: " . $_POST['langue-maternelle']."\n"
                              ."Autre langue maternelle: " . $_POST['champ-autre-maternelle']."\n";
				
			fwrite($fichierFormulaire,$contenu);
			fclose($fichierFormulaire);
	}
}
    function verificationDonnees(){

        $champsValides = empty($_POST["nom"])||empty($_POST["date-naissance"])||empty($_POST["prenom"])||empty($_POST["code-permanent2"])||empty($_POST["citoyennete"])
                       ||empty($_POST["lieu-naissance"])||empty($_POST["adresse"])||empty($_POST["sexe"])
                       ||empty($_POST["langue-usage"])||empty($_POST["langue-maternelle"])||empty($_POST["statut"]) || verifivationTroisNumerosVide();
              
        $anneeValide      = validerDate($_POST["date-naissance"]);
        $codePer1Valide   = validerCodePer($_POST["code-permanent1"]);
        $codePer2Valide   = validerCodePer($_POST["code-permanent2"]);
        $nasValide        = validerNas($_POST["nas"]);
        $numeroValide1    = validerTelephone($_POST["tel-dom"]);
        $numeroValide2    = validerTelephone($_POST["tel-cel"]);
        $numeroValide3    = validerTelephone($_POST["tel-trv"]);
        $pereValide       = validerParent($_POST["nom-pere"],$_POST["prenom-pere"]);
        $mereValide       = validerParent($_POST["nom-mere"],$_POST["prenom-mere"]);
        $codePostal1      = validerCodePostal($_POST["code-postal1"]);
        $codePostal2      = validerCodePostal($_POST["code-postal2"]);
        $courriel         = validerCourriel($_POST["courriel"]);
 
        $autreCitoyennete = validerAutreChamp(empty($_POST["citoyennete"]),"citoyennete" , "autre","autre-citoyennete");
        $autreLangueMaternelle = validerAutreChamp(empty($_POST["langue-maternelle"]), "langue-maternelle", "autre", "champ-autre-maternelle");
        $autreLangueMaternelle = validerAutreChamp(empty($_POST["langue-usage"]), "langue-usage", "autre", "champ-autre-usage");
        return $champsValides || !$anneeValide|| !$nasValide ||!$codePer1Valide || !$codePer2Valide || !$numeroValide1 ||!$autreCitoyennete|| !$autreLangueMaternelle
                || !$numeroValide2 || !$numeroValide3 || !$pereValide || !$mereValide || !$codePostal1 || !$codePostal2 || !$courriel;
    }
    
    function validerAutreChamp($empty, $nomRadio, $autre, $autreChamp){
        $valid=false;   
        
        if (!$empty){
            if($_POST[$nomRadio]==$autre){
                $valid= !empty($_POST[$autreChamp]);
            }else{
                $valid=true;
            }
        }
        
        return $valid;
    }
    function validerCourriel($courriel){
        if (!empty($courriel)){
            if (!filter_var($courriel, FILTER_VALIDATE_EMAIL)) {
                return false;
            }
        }
        return true;
    }
    
    function validerCodePostal($code){
        if (!empty($code)){
            $lettre1  = substr($code,0,1);
            $chiffre1 = substr($code,1,1);
            $lettre2  = substr($code,2,1);
            $espace   = substr($code,3,1);
            $chiffre2 = substr($code,4,1);
            $lettre3  = substr($code,5,1);
            $chiffre3 = substr($code,6,1);
            $formatCp = ctype_upper($lettre1)&& ctype_upper($lettre2)&&ctype_upper($lettre3)
                        &&ctype_digit($chiffre1)&&ctype_digit($chiffre2)&&ctype_digit($chiffre3) && $espace==' ';
            if (!$formatCp){            
                return false;           
            }
            return true;
        }
        return true;
    }
    
    function validerParent($nom,$prenom){
        if (!empty($nom)){
            if (empty($prenom)){
                return false;
            }
        }
        if (!empty($prenom)){
            if (empty($nom)){
                return false;
            }
        }
        return true;
    }
    
    function estBissextile($annee) {
        return (($annee % 4 === 0 && $annee % 100 > 0) || ($annee % 400 === 0));
    }
    function validerDate($date){
        $formatAnnee = true;
        $anneeValide = true;
        $moisValide  = true;
        $jourValide  = true;
        $fevrier29   = true;
        $fevrier28   = true;
        $annee = intval(substr($date,0,4));
        $mois  = intval(substr($date,5,7));
        $jour  = intval(substr($date,8,10));
        $tiret1      = substr($date,4,1);
    $tiret2      = substr($date,7,1);
     
        $anneeBissextile= estBissextile($annee);
        if ($tiret1 !='-' || $tiret2 != '-'){
            $formatAnnee= false;
    }
        if ($annee > (date("Y")-15)){
            $anneeValide=false;
        }    
        if($mois<1||$mois>12){
            $moisValide= false;
        }
        if ($jour<1||$jour>31){
            $jourValide= false;
        }
        if ($anneeBissextile){ 
           if($mois==2 && $jour>29){        
                $fevrier28 = false;
            }
        }
        if (!$anneeBissextile){  
            if($mois==2 && $jour>28){        
                $fevrier29 = false;
            }
        } 
        return $anneeValide && $moisValide && $jourValide && $fevrier29 && $fevrier28  && $formatAnnee;
    }   
    
    function validerCodePer($codePermanent){
        if (!empty($codePermanent)){
            $partie1 = substr($codePermanent,0,4);
            $partie2 = substr($codePermanent,4);
            for($i=0; $i<strlen($partie1);$i++){
                if (strlen($partie1)!= 4 || ($partie1[$i]<'A' || $partie1[$i]>'Z') ){
                    return false;  
                }
            }
            for($i=0; $i<strlen($partie2);$i++){
                if (strlen($partie2)!= 8 || ($partie2[$i]<'0' || $partie2[$i]>'9') ){
                    return false;  
                }
            }
        }
        return true; 
    }
    function validerTelephone($numero){
        if (!empty($numero)){
            $partie1 = substr($numero,0,3);
            $partie2 = substr($numero,4,3);
            $partie3 = substr($numero,8);
            $tiret1 = substr($numero,3,1);
            $tiret2 = substr($numero,7,1);
            for($i=0; $i<strlen($partie1);$i++){
                if (strlen($partie1)!= 3 || ($partie1[$i]<'0' || $partie1[$i]>'9') ){
                    return false;  
                }
            }
            for($i=0; $i<strlen($partie2);$i++){
                if (strlen($partie2)!= 3 || ($partie2[$i]<'0' || $partie2[$i]>'9') ){
                    return false;  
                }
            }
            for($i=0; $i<strlen($partie3);$i++){
                if (strlen($partie3)!= 4 || ($partie3[$i]<'0' || $partie3[$i]>'9') ){
                    return false;  
                }
            }
            if ($tiret1 !='-' || $tiret2 != '-'){
                return false;
            }
        }
        return true;
    }
    
    function validerNas($nas){
        if (!empty($nas)){
            for($i=0; $i<strlen($nas);$i++){
                if (strlen($nas)!= 9 || ($nas[$i]<'0' || $nas[$i]>'9') ){
                    return false;  
                } 
            }
        }
        return true;
    }
    
    function verifivationTroisNumerosVide(){      
        if(!empty($_POST["tel-dom"]) || !empty($_POST["tel-cel"]) || !empty($_POST["tel-trv"])){
            return false;
        }else{
            return true;
        }
    } 
    
    redirection();
?>